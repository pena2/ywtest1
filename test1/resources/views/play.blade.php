<!DOCTYPE html>
<html>
<head>
	<title>Test 1 - Play</title>
</head>
<body>




<div>
	<h1>Test 1: Card Game</h1>

	<form action="" method="post">
        <!-- <h3>got_it: <?php var_dump( $data['got_it']); ?></h3> -->

        <?php if ( $data['got_it'] ): ?>
            <div style="background-color: #fc0; padding: 14px;">
                You GOT IT! The Chamnce was <?php echo $data['prob']; ?>%.
                <br>
                <a href="/"><button type="button">RESET</button></a>
            </div>
        <?php else: ?>
            <div>Probability for the next one: <?php echo $data['prob']; ?>.</div>
        <?php endif; ?>


        <h3>Current Deck</h3>
        <div>
            <?php echo implode(', ', $data['current_deck']->__toArray() ); ?>
            <br>total: <?php echo count($data['current_deck']->__toArray() ); ?>
        </div>

        <h3>Drafted Deck</h3>
        <?php if ($data['drafted']): ?><p>Drafted: <?php echo $data['drafted']; ?></p><?php endif; ?>
        <div>
            <?php echo implode(', ', $data['drafted_deck']->__toArray() ); ?>
            <br>total: <?php echo count($data['drafted_deck']->__toArray() ); ?>
        </div>

        <?php if ( !$data['got_it'] ): ?>

		<h4>Suit / Rank</h4>
        <div>
            <select name="suit">
            <?php foreach($data['all_suits'] as $titem): ?>
            <option <?php if ( $data['rsuit']==$titem ) { ?>selected="selected"<?php } ?> value="<?php echo $titem; ?>"> <?php echo $titem; ?></option>
            <?php endforeach; ?>
            </select>

            <select name="rank">
            <?php foreach($data['all_ranks'] as $titem): ?>
            <option <?php if ( $data['rrank']==$titem ) { ?>selected="selected"<?php } ?> value="<?php echo $titem; ?>"> <?php echo $titem; ?></option>
            <?php endforeach; ?>
            </select>
        </div>

        <div>
            <button type="submit">DRAFT CARD</button>
            <a href="/"><button type="button">RESET</button></a>
        </div>

        <input type="hidden" name="current_deck" size="50" value="<?php echo implode(',', $data['current_deck']->__toArray() ); ?>">
        <br>
        <input type="hidden" name="drafted_deck" size="50" value="<?php echo implode(',', $data['drafted_deck']->__toArray() ); ?>">
        <input type="hidden" name="go" value="1">
        {{ csrf_field() }}


        <?php endif; ?>


	</form>


</div>


</body>
</html>