<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlayController extends Controller
{

    public function play(Request $req) {
        $tprob= 0;
        $tgot_it= false;
        $aa= $req->post();
        $tdrafted= null;

        $tcurrent_deck= $req->input('current_deck',null);
        $tcurrent_deck= $tcurrent_deck ? explode(',', $tcurrent_deck) : null;
        $tcurrent_deck= \App\Deck::new_from_cards($tcurrent_deck);

        $tdrafted_deck= $req->input('drafted_deck',null);
        $tdrafted_deck= $tdrafted_deck ? explode(',', $tdrafted_deck) : [];
        $tdrafted_deck= \App\Deck::new_from_cards($tdrafted_deck);

        $rsuit= $req->input('rsuit',null);
        $rrank= $req->input('rrank',null);

        // if no deck
        if ( !$tcurrent_deck || count($tcurrent_deck->__toArray()) < 1 || is_null($tdrafted_deck) ) {
            $tcurrent_deck= new \App\Deck(false, true);
            $tdrafted_deck= new \App\Deck(true);
        }

        if ( '1'==$req->input('go') ) {
            $rsuit= $req->input('suit');
            $rrank= $req->input('rank');
            $tdrafted= $tcurrent_deck->draft_card();
            $tdrafted_deck->insert($tdrafted);

            $tprob= count($tcurrent_deck->cards) > 0 ? 1/ count($tcurrent_deck->cards) : 0;
            if ( $rsuit == $tdrafted->suit && $rrank == $tdrafted->rank ) {
                $tgot_it= true;
            }

        }

        $tdata= [
            'all_suits' => \App\Card::all_suits(),
            'all_ranks' => \App\Card::all_ranks(),
            'current_deck' => $tcurrent_deck,
            'drafted_deck' => $tdrafted_deck,
            'drafted' => $tdrafted,
            'prob' => $tprob,
            'got_it' => $tgot_it,
            'rsuit' => $rsuit,
            'rrank' => $rrank,
        ];
        // echo "<pre>";echo($rrank); echo "</pre>";
        return view('play', ['data' => $tdata]);
    }


}
