<?php

namespace App;

use App\Card;

use Illuminate\Database\Eloquent\Model;

class Deck extends Model
{
    public $cards= [];

    public static function new_from_cards($rcards) {
    	if (!is_array($rcards)) {
            throw new Exception("New From Cards: Invalid cards array", 1);
            
        };
    	$aa= new self(true);
    	foreach ($rcards as $key => $value) {

    		$tcard= new Card( substr($value,0,1), substr($value,1,1) );
    		$aa->insert($tcard);
    	}
    	return $aa;
    }
    public function insert($rcard) {
    	$this->cards[]= $rcard;
    }


    public function draft_card() {
        if (!is_array($this->cards)) {
            throw new Exception("Draft Card: Invalid cards Array", 1);
        }
        if ( count($this->cards) == 0 ) {
            throw new Exception("Draft Card: No more cards in deck!", 1);
        }
    	$ttotal= count($this->cards);
    	$trandix= rand(0,$ttotal-1);
    	// echo "<pre>";var_dump($this->cards);echo "</pre>";die();
    	$tdrafted= $this->cards[$trandix];
    	unset($this->cards[$trandix]);
    	$tt= [];
    	foreach ($this->cards as $key => $value) { // keep good indexes
    		$tt[]= $value;
    	}
    	$this->cards= $tt;
    	return $tdrafted;
    }


    public function __construct($empty=false, $random= false){
    	$tcards= [];

    	if (!$empty) {
	    	foreach (Card::all_suits() as $key => $value) {
	    		foreach (Card::all_ranks() as $key2 => $value2) {
	    			$tcards[]= new Card($value,$value2);
	    		}
	    	}
	    	if ($random) {
		    	shuffle($tcards);
	    	}
    	}
    	$this->cards= $tcards;
    	// return $tcards;
    }

    public function __toString(){
    	return implode(", ", $this->cards);
    }
    public function __toArray(){
    	$rett= [];
    	foreach ($this->cards as $key => $value) {
    		$rett[]= "".$value;
    	}
    	return $rett;
    }


}
