<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Char extends Model
{
	public $symbol;
	public $times_encountered= 0;
	public $before= [];
	public $after= [];
	public $longest_distance= 0;

    public function __construct(String $char) {
    	$this->symbol= $char;
    }


    public function analyze(Phrase $phrase) {

    	// get times_encountered
        foreach (str_split($phrase->phrase) as $key => $value) {
            if (' '!= $value &&  $value == $this->symbol) {
                $this->times_encountered++;
            }
        }

        // get before, after, longest distance
        // ..


    	$rett= [
    		'symbol' => $this->symbol,
    		'times_encountered' => $this->times_encountered,
    		'before' => $this->before,
    		'after' => $this->after,
    		'longest_distance' => $this->longest_distance,
    	];
    	return $rett;
    }

}
