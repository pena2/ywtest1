<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phrase extends Model
{
	public $phrase;
	public $chars= [];

    public function __construct(String $text) {
    	$this->phrase= $text;

    	$chars1= str_split($text);

    	$tchars= [];
    	foreach ( $chars1  as $key => $value) {
    		if ( !in_array($value, $tchars) && ' ' != $value ) {
	    		$tchars[]= ($value);
    		}
    	}
    	foreach ($tchars as $key => $value) {
    		$this->chars[]= new Char($value);
    	}

    }

    public function analyze() {
    	$rett= [];

    	foreach ($this->chars as $key => $value) {
    		$rett[]= $value->analyze( $this );
    	}

    	return $rett;
    }

}
