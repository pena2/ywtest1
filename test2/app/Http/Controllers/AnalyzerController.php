<?php

namespace App\Http\Controllers;

use App\Phrase;
use App\Char;

use Illuminate\Http\Request;

class AnalyzerController extends Controller
{
    public function analyzer(Request $req) {
    	$tdata= [
    		'phrase' => $req->input('phrase')
    	];


    	$tdata['analyze']= null;

        if ( '1'==$req->input('go') ) {
        	$phrase = new Phrase( $req->input('phrase') );
    		$tdata['analyze']= $phrase->analyze();
		}


        return view('analyzer', ['data' => $tdata]);
    }
}
