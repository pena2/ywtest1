<!DOCTYPE html>
<html>
<head>
	<title>Test 2 - Analyzer</title>
</head>
<body>


	<div>
		<h1>Test 2: Phrase Analyzer</h1>
	</div>


	<form accept="" method="post">
		Enter your phrase: <input type="text" size="21" maxlength="255" name="phrase" value="<?php echo $data['phrase'] ?>">
		<button type="submit">Submit</button>
		<a href="/"><button type="button">RESET</button></a>

        <input type="hidden" name="go" value="1">
        {{ csrf_field() }}
	</form>


	<?php if ($data['analyze']): ?>
		<div>
			<table border="1">
				<tr>
					<th>symbol</th>
					<th>times encountered</th>
					<th>before</th>
					<th>after</th>
				</tr>
				<?php foreach( $data['analyze'] as $key => $value ): ?>
					<tr>
						<td><?php echo $value['symbol']; ?></td>
						<td><?php echo $value['times_encountered']; ?></td>
						<td><?php echo implode(', ', $value['before']) ; ?></td>
						<td><?php echo implode(', ', $value['after']) ; ?></td>
					</tr>
				<?php endforeach; ?>

			</table>

			<?php echo "<pre>";var_dump($data['analyze']); echo "</pre>"; ?>

		</div>
	<?php endif; ?>



</body>
</html>